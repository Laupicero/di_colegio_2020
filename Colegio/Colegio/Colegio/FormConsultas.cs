﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio
{
    public partial class FormConsultas : Form
    {
        public FormConsultas()
        {
            InitializeComponent();
        }

        // EVENTO RADIOBUTTON

        //Seleccion Alumno
        private void rbAlumno_CheckedChanged(object sender, EventArgs e)
        {
            cbBusqueda.Items.Clear();
            cbBusqueda.Text = "DNI Alumnos";

            StreamReader ficheroAlumnos = null;

            try
            {
                ficheroAlumnos = File.OpenText("Alumnos.txt");
                String linea = ficheroAlumnos.ReadLine();

                while (linea != null)
                {
                    String[] camposLinea = linea.Split('#');
                    cbBusqueda.Items.Add(camposLinea[0]);
                    linea = ficheroAlumnos.ReadLine();
                }
                ficheroAlumnos.Close();


            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message, "Error-Fichero no encontrado");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");

            }
            finally
            {
                if (ficheroAlumnos != null)
                {
                    ficheroAlumnos.Close();
                }

            }
        }

        //Seleccion Profesor
        private void rbProfesor_CheckedChanged(object sender, EventArgs e)
        {
            cbBusqueda.Items.Clear();
            cbBusqueda.Text = "DNI Profesores";

            StreamReader ficheroProfesores;

            try
            {
                ficheroProfesores = File.OpenText("Profesores.txt");
                String linea = ficheroProfesores.ReadLine();

                while (linea != null)
                {
                    String[] camposLinea = linea.Split('#');
                    cbBusqueda.Items.Add(camposLinea[0]);
                    linea = ficheroProfesores.ReadLine();
                }

                ficheroProfesores.Close();

            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message, "Error-Fichero no encontrado");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        //Seleccion Asignatura
        private void rbAsignatura_CheckedChanged(object sender, EventArgs e)
        {
            cbBusqueda.Items.Clear();
            cbBusqueda.Text = "ID Asignaturas";

            StreamReader ficheroAsign = null;

            try
            {
                ficheroAsign = File.OpenText("Asignaturas.txt");
                String linea = ficheroAsign.ReadLine();

                while (linea != null)
                {
                    String[] camposLinea = linea.Split('#');
                    cbBusqueda.Items.Add(camposLinea[0]);
                    //Ver asignaturas por nombre
                    //cbDn_iId.Items.Add(camposLinea[1]);
                    linea = ficheroAsign.ReadLine();
                }
                ficheroAsign.Close();

            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message, "Error-Fichero no encontrado");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");

            }
            finally
            {
                if (ficheroAsign != null)
                {
                    ficheroAsign.Close();
                }
            }
        }



        //-----------------------
        // Selección del ComboBox
        private void cbBusqueda_SelectedIndexChanged(object sender, EventArgs e)
        {
            borrarCampos();
            StreamReader fichero = null;

            //Rb-Alumno
            if (rbAlumno.Checked)
            {
                String dniSelec = (String)cbBusqueda.SelectedItem;

                try
                {
                    fichero = File.OpenText("Alumnos.txt");
                    String linea = fichero.ReadLine();

                    while (linea != null)
                    {

                        String[] camposLinea = linea.Split('#');

                        if (dniSelec == camposLinea[0])
                        {
                            for (int i = 0; i < camposLinea.Length; i++)
                            {

                                switch (i)
                                {
                                    case 0:
                                        lb1_2.Text = camposLinea[i];
                                        break;

                                    case 1:
                                        lb2_2.Text = camposLinea[i];
                                        break;

                                    case 2:
                                        lb3_2.Text = camposLinea[i];
                                        break;

                                    case 3:
                                        lb4_2.Text = camposLinea[i];
                                        break;

                                    case 4:
                                        lb5_2.Text = camposLinea[i];
                                        break;
                                }
                            }
                            //Rellenar labels
                            lb1_1.Text = "DNI";
                            lb2_1.Text = "Nombre";
                            lb3_1.Text = "Fecha";
                            lb4_1.Text = "Dirección";
                            lb5_1.Text = "Teléfono";
                        }
                        linea = fichero.ReadLine();
                    }

                }
                catch (FileNotFoundException ex)
                {
                    MessageBox.Show(ex.Message, "Error-Fichero no encontrado");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");

                }
                finally
                {
                    if (fichero != null)
                    {
                        fichero.Close();
                    }

                }
                //RB-Profesor Seleccionado
            }
            else if (rbProfesor.Checked)
            {
                String dniSelec = (String)cbBusqueda.SelectedItem;

                try
                {
                    fichero = File.OpenText("Profesores.txt");
                    String linea = fichero.ReadLine();

                    while (linea != null)
                    {
                        String[] camposLinea = linea.Split('#');

                        if (dniSelec == camposLinea[0])
                        {
                            for (int i = 0; i < camposLinea.Length; i++)
                            {

                                switch (i)
                                {
                                    case 0:
                                        lb1_2.Text = camposLinea[i];
                                        break;

                                    case 1:
                                        lb2_2.Text = camposLinea[i];
                                        break;

                                    case 2:
                                        lb3_2.Text = camposLinea[i];
                                        break;

                                    case 3:
                                        lb4_2.Text = camposLinea[i];
                                        break;

                                    case 4:
                                        lb5_2.Text = camposLinea[i];
                                        break;

                                    case 5:
                                        lb6_2.Text = camposLinea[i];
                                        break;
                                }
                            }
                            //Rellenar labels
                            lb1_1.Text = "DNI";
                            lb2_1.Text = "Nombre";
                            lb3_1.Text = "Dirección";
                            lb4_1.Text = "Asignatura";
                            lb5_1.Text = "Estudios";
                            lb6_1.Text = "Teléfono";
                        }
                        linea = fichero.ReadLine();
                    }

                }
                catch (FileNotFoundException ex)
                {
                    MessageBox.Show(ex.Message, "Error-Fichero no encontrado");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");

                }
                finally
                {
                    if (fichero != null)
                    {
                        fichero.Close();
                    }
                }

            }
            else
            {
                String idSelec = (String)cbBusqueda.SelectedItem;

                try
                {
                    fichero = File.OpenText("Asignaturas.txt");
                    String linea = fichero.ReadLine();

                    while (linea != null)
                    {
                        //Guardo solo una línea, no todas
                        String[] camposLinea = linea.Split('#');

                        if (idSelec == camposLinea[0])
                        {
                            for (int i = 0; i < camposLinea.Length; i++)
                            {

                                switch (i)
                                {
                                    case 0:
                                        lb1_2.Text = camposLinea[i];
                                        break;

                                    case 1:
                                        lb2_2.Text = camposLinea[i];
                                        break;

                                    case 2:
                                        lb3_2.Text = camposLinea[i];
                                        break;

                                    case 3:
                                        lb4_2.Text = camposLinea[i];
                                        break;
                                }
                            }
                            //Rellenar labels
                            lb1_1.Text = "ID";
                            lb2_1.Text = "Nombre";
                            lb3_1.Text = "Departamento";
                            lb4_1.Text = "Horas";
                        }
                        linea = fichero.ReadLine();
                    }

                }
                catch (FileNotFoundException ex)
                {
                    MessageBox.Show(ex.Message, "Error-Fichero no encontrado");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");

                }
                finally
                {
                    if (fichero != null)
                    {
                        fichero.Close();
                    }
                }
            }
        }

        private void borrarCampos()
        {
            lb1_1.Text = "";
            lb1_2.Text = "";
            lb2_1.Text = "";
            lb2_2.Text = "";
            lb3_1.Text = "";
            lb3_2.Text = "";
            lb4_1.Text = "";
            lb4_2.Text = "";
            lb5_1.Text = "";
            lb5_2.Text = "";
            lb6_1.Text = "";
            lb6_2.Text = "";
        }
    }
}
