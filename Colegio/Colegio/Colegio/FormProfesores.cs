﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio
{
    public partial class FormProfesores : Form
    {
        public FormProfesores()
        {
            InitializeComponent();
            rellenarComboAsignatura();
            cmbAsignaturaProfesor.Text = "Seleccione";
        }

        


        //BOTÓN GUARDAR PROFESOR
        private void btnGuardarProfesor_Click(object sender, EventArgs e)
        {
            if(camposRellenos() && DNIFormatoCorrecto() && tlfFormatoCorrecto())
            {
                StreamWriter ficheroProfes;
                String nombreFicheroProfesores = "Profesores.txt";

                ProfesorPOJO prof = new ProfesorPOJO(txtDniProfesor.Text, txtNombreProfesor.Text, txtDireccionProfesor.Text, txtTelefonoProfesor.Text, 
                    txtEstudioProfesor.Text, cmbAsignaturaProfesor.SelectedItem.ToString());

                String datosProfesor = prof.DNI+ "#" + prof.NOMBRE + "#" + prof.DIRECCION + "#" + prof.ESTUDIOS
                   + "#" + prof.ASIGNATURA_IMPARTIDA + "#" + prof.TLF;

                if (File.Exists(nombreFicheroProfesores))
                {
                    ficheroProfes = File.AppendText(nombreFicheroProfesores);
                    ficheroProfes.WriteLine(datosProfesor);
                    ficheroProfes.Close();

                }
                else
                {
                    ficheroProfes = File.CreateText(nombreFicheroProfesores);
                    ficheroProfes.WriteLine(datosProfesor);
                    ficheroProfes.Close();
                }

                //Limpiar campos
                limpiarCampos();
            }
            else
            {
                MessageBox.Show("Rellene todos los campos y establezaca bien los formatos", "ERROR");
            }
        }//Fin Guardar

        



        //BOTÓN LIMPIAR FORMULARIO
        private void btnLimpiarProfesor_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }   



        //---------------------
        //MÉTODOS AUXILIARES
        //---------------------
        private void rellenarComboAsignatura()
        {
            StreamReader ficheroAsig = null;

            try
            {
                ficheroAsig = File.OpenText("Asignaturas.txt");
                String linea = ficheroAsig.ReadLine();

                while (linea != null)
                {
                    String[] camposLinea = linea.Split('#');
                    cmbAsignaturaProfesor.Items.Add(camposLinea[1]);
                    linea = ficheroAsig.ReadLine();
                }
                ficheroAsig.Close();


            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show(ex.Message, "Error-Fichero no encontrado");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");

            }
            finally
            {
                if (ficheroAsig != null)
                    ficheroAsig.Close();

            }
        }//Fin rellenarComboAsignatura

        private void limpiarCampos()
        {
            txtTelefonoProfesor.Clear();
            txtNombreProfesor.Clear();
            txtEstudioProfesor.Clear();
            txtDniProfesor.Clear();
            txtDireccionProfesor.Clear();
            cmbAsignaturaProfesor.Text = "Seleccione";
        }

        //DE FORMATO:
        private bool DNIFormatoCorrecto()
        {
            return FiltrosRegex.validarDni(txtDniProfesor.Text);
        }

        private bool camposRellenos()
        {
            bool res = false;

            if (txtDniProfesor.TextLength > 0 && txtNombreProfesor.TextLength > 0 && txtDireccionProfesor.TextLength > 0 && txtEstudioProfesor.TextLength > 0 &&
                txtTelefonoProfesor.TextLength > 0 && cmbAsignaturaProfesor.SelectedIndex >= 0)
                res = true;

            return res;
        }


        private bool tlfFormatoCorrecto()
        {
            return FiltrosRegex.validarTelefono(txtTelefonoProfesor.Text);
        }
    }
}
