﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio
{
    public partial class FormColegio : Form
    {
        //Atributos de clase que son instancias de los formularios
        //Para que cada vez que seleccionemos no nos cree una instancia nueva y siempre
        // trabaje con la misma
        private FormAlumnos ventanaAlumnos;
        private FormAsignaturas ventanaAsignaturas;
        private FormConsultas ventanaConsultas;

        //Constructor
        public FormColegio()
        {
            InitializeComponent();
            this.ventanaAlumnos = new FormAlumnos();
            this.ventanaAsignaturas = new FormAsignaturas();
            this.ventanaConsultas = new FormConsultas();
        }


        //BOTONES DEL FORMULARIO

        //BOTÓN SALIR
        private void salidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }


        //BOTÓN FORMULARIO INSERTAR ALUMNOS
        private void alumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ventanaAlumnos.MdiParent = this;
            this.ventanaAlumnos.WindowState = FormWindowState.Maximized;
            this.ventanaAlumnos.Show();
        }

        //BOTÓN FORMULARIO INSERTAR PROFESORES
        //Aquí nos crearemos siempre un formulario dado que actualizaremos el comboBox de las asignaturas con su respectivo fichero
        private void profesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormProfesores ventanaProfesores = new FormProfesores();
            ventanaProfesores.MdiParent = this;
            ventanaProfesores.WindowState = FormWindowState.Maximized;
            ventanaProfesores.Show();
        }

        //BOTÓN FORMULARIO INSERTAR ASIGNATURAS
        private void asignaturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ventanaAsignaturas.MdiParent = this;
            this.ventanaAsignaturas.WindowState = FormWindowState.Maximized;
            this.ventanaAsignaturas.Show();
        }


        //BOTÓN FORMULARIO REALIZAR CONSULTAS
        private void consultasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ventanaConsultas.MdiParent = this;
            this.ventanaConsultas.WindowState = FormWindowState.Maximized;
            this.ventanaConsultas.Show();
        }
    }
}
