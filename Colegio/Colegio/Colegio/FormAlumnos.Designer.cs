﻿
namespace Colegio
{
    partial class FormAlumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAlumnos));
            this.lblDNI = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDniAlumno = new System.Windows.Forms.TextBox();
            this.txtNombreAlumno = new System.Windows.Forms.TextBox();
            this.txtDireccionAlumno = new System.Windows.Forms.TextBox();
            this.txtTelefonoAlumno = new System.Windows.Forms.TextBox();
            this.dtpFechaNacAlumno = new System.Windows.Forms.DateTimePicker();
            this.pictureBoxAlumnos = new System.Windows.Forms.PictureBox();
            this.btnGuardarAlumno = new System.Windows.Forms.Button();
            this.btnLimpiarAlumno = new System.Windows.Forms.Button();
            this.gbAlumnos = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlumnos)).BeginInit();
            this.gbAlumnos.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblDNI
            // 
            this.lblDNI.AutoSize = true;
            this.lblDNI.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDNI.Location = new System.Drawing.Point(6, 37);
            this.lblDNI.Name = "lblDNI";
            this.lblDNI.Size = new System.Drawing.Size(44, 17);
            this.lblDNI.TabIndex = 0;
            this.lblDNI.Text = "D.N.I";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fecha";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Direccion";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Telefono";
            // 
            // txtDniAlumno
            // 
            this.txtDniAlumno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDniAlumno.Location = new System.Drawing.Point(138, 37);
            this.txtDniAlumno.Name = "txtDniAlumno";
            this.txtDniAlumno.Size = new System.Drawing.Size(247, 23);
            this.txtDniAlumno.TabIndex = 5;
            // 
            // txtNombreAlumno
            // 
            this.txtNombreAlumno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreAlumno.Location = new System.Drawing.Point(138, 75);
            this.txtNombreAlumno.Name = "txtNombreAlumno";
            this.txtNombreAlumno.Size = new System.Drawing.Size(247, 23);
            this.txtNombreAlumno.TabIndex = 6;
            // 
            // txtDireccionAlumno
            // 
            this.txtDireccionAlumno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccionAlumno.Location = new System.Drawing.Point(138, 156);
            this.txtDireccionAlumno.Name = "txtDireccionAlumno";
            this.txtDireccionAlumno.Size = new System.Drawing.Size(247, 23);
            this.txtDireccionAlumno.TabIndex = 7;
            // 
            // txtTelefonoAlumno
            // 
            this.txtTelefonoAlumno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefonoAlumno.Location = new System.Drawing.Point(138, 197);
            this.txtTelefonoAlumno.Name = "txtTelefonoAlumno";
            this.txtTelefonoAlumno.Size = new System.Drawing.Size(247, 23);
            this.txtTelefonoAlumno.TabIndex = 8;
            // 
            // dtpFechaNacAlumno
            // 
            this.dtpFechaNacAlumno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaNacAlumno.Location = new System.Drawing.Point(138, 115);
            this.dtpFechaNacAlumno.Name = "dtpFechaNacAlumno";
            this.dtpFechaNacAlumno.Size = new System.Drawing.Size(247, 23);
            this.dtpFechaNacAlumno.TabIndex = 9;
            // 
            // pictureBoxAlumnos
            // 
            this.pictureBoxAlumnos.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAlumnos.Image")));
            this.pictureBoxAlumnos.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxAlumnos.Name = "pictureBoxAlumnos";
            this.pictureBoxAlumnos.Size = new System.Drawing.Size(279, 313);
            this.pictureBoxAlumnos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAlumnos.TabIndex = 10;
            this.pictureBoxAlumnos.TabStop = false;
            // 
            // btnGuardarAlumno
            // 
            this.btnGuardarAlumno.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnGuardarAlumno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarAlumno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarAlumno.Location = new System.Drawing.Point(308, 293);
            this.btnGuardarAlumno.Name = "btnGuardarAlumno";
            this.btnGuardarAlumno.Size = new System.Drawing.Size(196, 32);
            this.btnGuardarAlumno.TabIndex = 11;
            this.btnGuardarAlumno.Text = "GUARDAR";
            this.btnGuardarAlumno.UseVisualStyleBackColor = false;
            this.btnGuardarAlumno.Click += new System.EventHandler(this.btnGuardarAlumno_Click);
            // 
            // btnLimpiarAlumno
            // 
            this.btnLimpiarAlumno.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnLimpiarAlumno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpiarAlumno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiarAlumno.Location = new System.Drawing.Point(558, 293);
            this.btnLimpiarAlumno.Name = "btnLimpiarAlumno";
            this.btnLimpiarAlumno.Size = new System.Drawing.Size(202, 32);
            this.btnLimpiarAlumno.TabIndex = 12;
            this.btnLimpiarAlumno.Text = "LIMPIAR";
            this.btnLimpiarAlumno.UseVisualStyleBackColor = false;
            this.btnLimpiarAlumno.Click += new System.EventHandler(this.btnLimpiarAlumno_Click);
            // 
            // gbAlumnos
            // 
            this.gbAlumnos.Controls.Add(this.lblDNI);
            this.gbAlumnos.Controls.Add(this.label1);
            this.gbAlumnos.Controls.Add(this.label2);
            this.gbAlumnos.Controls.Add(this.label3);
            this.gbAlumnos.Controls.Add(this.dtpFechaNacAlumno);
            this.gbAlumnos.Controls.Add(this.label4);
            this.gbAlumnos.Controls.Add(this.txtTelefonoAlumno);
            this.gbAlumnos.Controls.Add(this.txtDniAlumno);
            this.gbAlumnos.Controls.Add(this.txtDireccionAlumno);
            this.gbAlumnos.Controls.Add(this.txtNombreAlumno);
            this.gbAlumnos.Location = new System.Drawing.Point(308, 12);
            this.gbAlumnos.Name = "gbAlumnos";
            this.gbAlumnos.Size = new System.Drawing.Size(452, 251);
            this.gbAlumnos.TabIndex = 13;
            this.gbAlumnos.TabStop = false;
            this.gbAlumnos.Text = "Datos Alumnos";
            // 
            // FormAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 361);
            this.ControlBox = false;
            this.Controls.Add(this.gbAlumnos);
            this.Controls.Add(this.btnLimpiarAlumno);
            this.Controls.Add(this.btnGuardarAlumno);
            this.Controls.Add(this.pictureBoxAlumnos);
            this.Name = "FormAlumnos";
            this.Text = "alumnos";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAlumnos)).EndInit();
            this.gbAlumnos.ResumeLayout(false);
            this.gbAlumnos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDNI;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDniAlumno;
        private System.Windows.Forms.TextBox txtNombreAlumno;
        private System.Windows.Forms.TextBox txtDireccionAlumno;
        private System.Windows.Forms.TextBox txtTelefonoAlumno;
        private System.Windows.Forms.DateTimePicker dtpFechaNacAlumno;
        private System.Windows.Forms.PictureBox pictureBoxAlumnos;
        private System.Windows.Forms.Button btnGuardarAlumno;
        private System.Windows.Forms.Button btnLimpiarAlumno;
        private System.Windows.Forms.GroupBox gbAlumnos;
    }
}