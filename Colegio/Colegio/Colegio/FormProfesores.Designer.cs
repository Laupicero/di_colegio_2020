﻿
namespace Colegio
{
    partial class FormProfesores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProfesores));
            this.btnLimpiarProfesor = new System.Windows.Forms.Button();
            this.btnGuardarProfesor = new System.Windows.Forms.Button();
            this.pictureBoxProfesores = new System.Windows.Forms.PictureBox();
            this.txtTelefonoProfesor = new System.Windows.Forms.TextBox();
            this.txtDireccionProfesor = new System.Windows.Forms.TextBox();
            this.txtNombreProfesor = new System.Windows.Forms.TextBox();
            this.txtDniProfesor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDNI = new System.Windows.Forms.Label();
            this.txtEstudioProfesor = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbAsignaturaProfesor = new System.Windows.Forms.ComboBox();
            this.gbProfesores = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProfesores)).BeginInit();
            this.gbProfesores.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLimpiarProfesor
            // 
            this.btnLimpiarProfesor.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnLimpiarProfesor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpiarProfesor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiarProfesor.Location = new System.Drawing.Point(538, 304);
            this.btnLimpiarProfesor.Name = "btnLimpiarProfesor";
            this.btnLimpiarProfesor.Size = new System.Drawing.Size(203, 32);
            this.btnLimpiarProfesor.TabIndex = 25;
            this.btnLimpiarProfesor.Text = "LIMPIAR";
            this.btnLimpiarProfesor.UseVisualStyleBackColor = false;
            this.btnLimpiarProfesor.Click += new System.EventHandler(this.btnLimpiarProfesor_Click);
            // 
            // btnGuardarProfesor
            // 
            this.btnGuardarProfesor.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnGuardarProfesor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarProfesor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarProfesor.Location = new System.Drawing.Point(287, 304);
            this.btnGuardarProfesor.Name = "btnGuardarProfesor";
            this.btnGuardarProfesor.Size = new System.Drawing.Size(197, 32);
            this.btnGuardarProfesor.TabIndex = 24;
            this.btnGuardarProfesor.Text = "GUARDAR";
            this.btnGuardarProfesor.UseVisualStyleBackColor = false;
            this.btnGuardarProfesor.Click += new System.EventHandler(this.btnGuardarProfesor_Click);
            // 
            // pictureBoxProfesores
            // 
            this.pictureBoxProfesores.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxProfesores.Image")));
            this.pictureBoxProfesores.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxProfesores.Name = "pictureBoxProfesores";
            this.pictureBoxProfesores.Size = new System.Drawing.Size(257, 324);
            this.pictureBoxProfesores.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxProfesores.TabIndex = 23;
            this.pictureBoxProfesores.TabStop = false;
            // 
            // txtTelefonoProfesor
            // 
            this.txtTelefonoProfesor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefonoProfesor.Location = new System.Drawing.Point(150, 140);
            this.txtTelefonoProfesor.Name = "txtTelefonoProfesor";
            this.txtTelefonoProfesor.Size = new System.Drawing.Size(247, 23);
            this.txtTelefonoProfesor.TabIndex = 21;
            // 
            // txtDireccionProfesor
            // 
            this.txtDireccionProfesor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccionProfesor.Location = new System.Drawing.Point(150, 104);
            this.txtDireccionProfesor.Name = "txtDireccionProfesor";
            this.txtDireccionProfesor.Size = new System.Drawing.Size(247, 23);
            this.txtDireccionProfesor.TabIndex = 20;
            // 
            // txtNombreProfesor
            // 
            this.txtNombreProfesor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreProfesor.Location = new System.Drawing.Point(150, 66);
            this.txtNombreProfesor.Name = "txtNombreProfesor";
            this.txtNombreProfesor.Size = new System.Drawing.Size(247, 23);
            this.txtNombreProfesor.TabIndex = 19;
            // 
            // txtDniProfesor
            // 
            this.txtDniProfesor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDniProfesor.Location = new System.Drawing.Point(150, 30);
            this.txtDniProfesor.Name = "txtDniProfesor";
            this.txtDniProfesor.Size = new System.Drawing.Size(247, 23);
            this.txtDniProfesor.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "Telefono";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "Direccion";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Nombre";
            // 
            // lblDNI
            // 
            this.lblDNI.AutoSize = true;
            this.lblDNI.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDNI.Location = new System.Drawing.Point(18, 30);
            this.lblDNI.Name = "lblDNI";
            this.lblDNI.Size = new System.Drawing.Size(44, 17);
            this.lblDNI.TabIndex = 13;
            this.lblDNI.Text = "D.N.I";
            // 
            // txtEstudioProfesor
            // 
            this.txtEstudioProfesor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstudioProfesor.Location = new System.Drawing.Point(150, 177);
            this.txtEstudioProfesor.Name = "txtEstudioProfesor";
            this.txtEstudioProfesor.Size = new System.Drawing.Size(247, 23);
            this.txtEstudioProfesor.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 17);
            this.label2.TabIndex = 26;
            this.label2.Text = "Asignatura ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 17);
            this.label5.TabIndex = 29;
            this.label5.Text = "Estudios";
            // 
            // cmbAsignaturaProfesor
            // 
            this.cmbAsignaturaProfesor.FormattingEnabled = true;
            this.cmbAsignaturaProfesor.Location = new System.Drawing.Point(150, 216);
            this.cmbAsignaturaProfesor.Name = "cmbAsignaturaProfesor";
            this.cmbAsignaturaProfesor.Size = new System.Drawing.Size(247, 21);
            this.cmbAsignaturaProfesor.TabIndex = 30;
            // 
            // gbProfesores
            // 
            this.gbProfesores.Controls.Add(this.lblDNI);
            this.gbProfesores.Controls.Add(this.cmbAsignaturaProfesor);
            this.gbProfesores.Controls.Add(this.label1);
            this.gbProfesores.Controls.Add(this.label5);
            this.gbProfesores.Controls.Add(this.label3);
            this.gbProfesores.Controls.Add(this.txtEstudioProfesor);
            this.gbProfesores.Controls.Add(this.label4);
            this.gbProfesores.Controls.Add(this.label2);
            this.gbProfesores.Controls.Add(this.txtDniProfesor);
            this.gbProfesores.Controls.Add(this.txtNombreProfesor);
            this.gbProfesores.Controls.Add(this.txtDireccionProfesor);
            this.gbProfesores.Controls.Add(this.txtTelefonoProfesor);
            this.gbProfesores.Location = new System.Drawing.Point(275, 22);
            this.gbProfesores.Name = "gbProfesores";
            this.gbProfesores.Size = new System.Drawing.Size(489, 259);
            this.gbProfesores.TabIndex = 0;
            this.gbProfesores.TabStop = false;
            this.gbProfesores.Text = "Datos Profesores";
            // 
            // FormProfesores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 361);
            this.ControlBox = false;
            this.Controls.Add(this.gbProfesores);
            this.Controls.Add(this.btnLimpiarProfesor);
            this.Controls.Add(this.btnGuardarProfesor);
            this.Controls.Add(this.pictureBoxProfesores);
            this.Name = "FormProfesores";
            this.Text = "Profesores";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProfesores)).EndInit();
            this.gbProfesores.ResumeLayout(false);
            this.gbProfesores.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLimpiarProfesor;
        private System.Windows.Forms.Button btnGuardarProfesor;
        private System.Windows.Forms.PictureBox pictureBoxProfesores;
        private System.Windows.Forms.TextBox txtTelefonoProfesor;
        private System.Windows.Forms.TextBox txtDireccionProfesor;
        private System.Windows.Forms.TextBox txtNombreProfesor;
        private System.Windows.Forms.TextBox txtDniProfesor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDNI;
        private System.Windows.Forms.TextBox txtEstudioProfesor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbAsignaturaProfesor;
        private System.Windows.Forms.GroupBox gbProfesores;
    }
}