﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio
{
    public partial class FormAsignaturas : Form
    {
        public FormAsignaturas()
        {
            InitializeComponent();
            rellenarComboAsignaturas();
            comboDep.Text = "Seleccione";
        }


        //BOTÓN GUARDAR ASIGNATURAS
        private void btnGuardarDatos_Click(object sender, EventArgs e)
        {
            if (comprobarCamposRellenos() && comprobarHorasAsign() && comprobarIDAsign())
            {
                StreamWriter ficheroAsig;
                String nombreFicheroAsig = "Asignaturas.txt";

                AsignaturaPOJO asig = new AsignaturaPOJO(txtIdAsig.Text, txtNombreAsig.Text, comboDep.SelectedItem.ToString(), txtHorasSemanales.Text);

                String datosAsignaturas = asig.ID + "#" + asig.NOMBRE + "#" + asig.DEPARTAMENTO + "#" + asig.HORAS_SEMANALES;

                if (File.Exists(nombreFicheroAsig))
                {
                    ficheroAsig = File.AppendText(nombreFicheroAsig);
                    ficheroAsig.WriteLine(datosAsignaturas);
                    ficheroAsig.Close();

                }
                else
                {
                    ficheroAsig = File.CreateText(nombreFicheroAsig);
                    ficheroAsig.WriteLine(datosAsignaturas);
                    ficheroAsig.Close();
                }
                //Limpiar campos
                borrarCamposAsignaturas();
            }
            else
            {
                MessageBox.Show("Compruebe el formato del ID y las horas de la asignatura." +
                    "\nSólo formato numérico", "ERROR");
            }
        }

        //BOTÓN LIMPIAR ASIGNATURAS
        private void btnLimpiarCampos_Click(object sender, EventArgs e)
        {
            borrarCamposAsignaturas();
        }



        //-------
        // MÉTODOS AUXILIARES

        private void borrarCamposAsignaturas()
        {
            txtHorasSemanales.Clear();
            txtIdAsig.Clear();
            txtNombreAsig.Clear();
            comboDep.Text = "Seleccione";
        }


        private void rellenarComboAsignaturas()
        {
            String[] dep = {"Lengua", "Inglés", "Matemáticas", "Ciencias", "Informática", "Artes Plásticas y Música"};
            comboDep.Items.AddRange(dep);
         
        }

        private bool comprobarCamposRellenos()
        {
            bool res = false;

            if (txtIdAsig.TextLength > 0 && txtNombreAsig.TextLength > 0 && txtHorasSemanales.TextLength > 0 && comboDep.SelectedIndex >= 0)
                res = true;
            else
                MessageBox.Show("Rellene todos los campos", "ERROR");

            return res;
        }

        private bool comprobarIDAsign()
        {
            return FiltrosRegex.validarIdAsignatura(txtIdAsig.Text);
        }

        private bool comprobarHorasAsign()
        {
            return FiltrosRegex.validarIdAsignatura(txtHorasSemanales.Text);
        }
    }
}
