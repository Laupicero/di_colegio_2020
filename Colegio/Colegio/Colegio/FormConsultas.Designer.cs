﻿namespace Colegio
{
    partial class FormConsultas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbSeleccion = new System.Windows.Forms.GroupBox();
            this.cbBusqueda = new System.Windows.Forms.ComboBox();
            this.rbAsignatura = new System.Windows.Forms.RadioButton();
            this.rbProfesor = new System.Windows.Forms.RadioButton();
            this.rbAlumno = new System.Windows.Forms.RadioButton();
            this.groupBoxDatos = new System.Windows.Forms.GroupBox();
            this.lb6_2 = new System.Windows.Forms.Label();
            this.lb5_2 = new System.Windows.Forms.Label();
            this.lb4_2 = new System.Windows.Forms.Label();
            this.lb3_2 = new System.Windows.Forms.Label();
            this.lb2_2 = new System.Windows.Forms.Label();
            this.lb1_2 = new System.Windows.Forms.Label();
            this.lb6_1 = new System.Windows.Forms.Label();
            this.lb5_1 = new System.Windows.Forms.Label();
            this.lb4_1 = new System.Windows.Forms.Label();
            this.lb3_1 = new System.Windows.Forms.Label();
            this.lb2_1 = new System.Windows.Forms.Label();
            this.lb1_1 = new System.Windows.Forms.Label();
            this.gbSeleccion.SuspendLayout();
            this.groupBoxDatos.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSeleccion
            // 
            this.gbSeleccion.Controls.Add(this.cbBusqueda);
            this.gbSeleccion.Controls.Add(this.rbAsignatura);
            this.gbSeleccion.Controls.Add(this.rbProfesor);
            this.gbSeleccion.Controls.Add(this.rbAlumno);
            this.gbSeleccion.Location = new System.Drawing.Point(24, 27);
            this.gbSeleccion.Name = "gbSeleccion";
            this.gbSeleccion.Size = new System.Drawing.Size(341, 301);
            this.gbSeleccion.TabIndex = 0;
            this.gbSeleccion.TabStop = false;
            this.gbSeleccion.Text = "Búsqueda";
            // 
            // cbBusqueda
            // 
            this.cbBusqueda.FormattingEnabled = true;
            this.cbBusqueda.Location = new System.Drawing.Point(7, 149);
            this.cbBusqueda.Name = "cbBusqueda";
            this.cbBusqueda.Size = new System.Drawing.Size(279, 21);
            this.cbBusqueda.TabIndex = 3;
            this.cbBusqueda.SelectedIndexChanged += new System.EventHandler(this.cbBusqueda_SelectedIndexChanged);
            // 
            // rbAsignatura
            // 
            this.rbAsignatura.AutoSize = true;
            this.rbAsignatura.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbAsignatura.Location = new System.Drawing.Point(6, 94);
            this.rbAsignatura.Name = "rbAsignatura";
            this.rbAsignatura.Size = new System.Drawing.Size(93, 23);
            this.rbAsignatura.TabIndex = 2;
            this.rbAsignatura.TabStop = true;
            this.rbAsignatura.Text = "Asignatura";
            this.rbAsignatura.UseVisualStyleBackColor = true;
            this.rbAsignatura.CheckedChanged += new System.EventHandler(this.rbAsignatura_CheckedChanged);
            // 
            // rbProfesor
            // 
            this.rbProfesor.AutoSize = true;
            this.rbProfesor.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbProfesor.Location = new System.Drawing.Point(6, 56);
            this.rbProfesor.Name = "rbProfesor";
            this.rbProfesor.Size = new System.Drawing.Size(79, 23);
            this.rbProfesor.TabIndex = 1;
            this.rbProfesor.TabStop = true;
            this.rbProfesor.Text = "Profesor";
            this.rbProfesor.UseVisualStyleBackColor = true;
            this.rbProfesor.CheckedChanged += new System.EventHandler(this.rbProfesor_CheckedChanged);
            // 
            // rbAlumno
            // 
            this.rbAlumno.AutoSize = true;
            this.rbAlumno.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbAlumno.Location = new System.Drawing.Point(7, 20);
            this.rbAlumno.Name = "rbAlumno";
            this.rbAlumno.Size = new System.Drawing.Size(75, 23);
            this.rbAlumno.TabIndex = 0;
            this.rbAlumno.TabStop = true;
            this.rbAlumno.Text = "Alumno";
            this.rbAlumno.UseVisualStyleBackColor = true;
            this.rbAlumno.CheckedChanged += new System.EventHandler(this.rbAlumno_CheckedChanged);
            // 
            // groupBoxDatos
            // 
            this.groupBoxDatos.Controls.Add(this.lb6_2);
            this.groupBoxDatos.Controls.Add(this.lb5_2);
            this.groupBoxDatos.Controls.Add(this.lb4_2);
            this.groupBoxDatos.Controls.Add(this.lb3_2);
            this.groupBoxDatos.Controls.Add(this.lb2_2);
            this.groupBoxDatos.Controls.Add(this.lb1_2);
            this.groupBoxDatos.Controls.Add(this.lb6_1);
            this.groupBoxDatos.Controls.Add(this.lb5_1);
            this.groupBoxDatos.Controls.Add(this.lb4_1);
            this.groupBoxDatos.Controls.Add(this.lb3_1);
            this.groupBoxDatos.Controls.Add(this.lb2_1);
            this.groupBoxDatos.Controls.Add(this.lb1_1);
            this.groupBoxDatos.Location = new System.Drawing.Point(395, 27);
            this.groupBoxDatos.Name = "groupBoxDatos";
            this.groupBoxDatos.Size = new System.Drawing.Size(392, 301);
            this.groupBoxDatos.TabIndex = 1;
            this.groupBoxDatos.TabStop = false;
            this.groupBoxDatos.Text = "Datos";
            // 
            // lb6_2
            // 
            this.lb6_2.AutoSize = true;
            this.lb6_2.Location = new System.Drawing.Point(174, 200);
            this.lb6_2.Name = "lb6_2";
            this.lb6_2.Size = new System.Drawing.Size(0, 13);
            this.lb6_2.TabIndex = 12;
            // 
            // lb5_2
            // 
            this.lb5_2.AutoSize = true;
            this.lb5_2.Location = new System.Drawing.Point(174, 167);
            this.lb5_2.Name = "lb5_2";
            this.lb5_2.Size = new System.Drawing.Size(0, 13);
            this.lb5_2.TabIndex = 11;
            // 
            // lb4_2
            // 
            this.lb4_2.AutoSize = true;
            this.lb4_2.Location = new System.Drawing.Point(174, 136);
            this.lb4_2.Name = "lb4_2";
            this.lb4_2.Size = new System.Drawing.Size(0, 13);
            this.lb4_2.TabIndex = 10;
            // 
            // lb3_2
            // 
            this.lb3_2.AutoSize = true;
            this.lb3_2.Location = new System.Drawing.Point(174, 104);
            this.lb3_2.Name = "lb3_2";
            this.lb3_2.Size = new System.Drawing.Size(0, 13);
            this.lb3_2.TabIndex = 9;
            // 
            // lb2_2
            // 
            this.lb2_2.AutoSize = true;
            this.lb2_2.Location = new System.Drawing.Point(174, 72);
            this.lb2_2.Name = "lb2_2";
            this.lb2_2.Size = new System.Drawing.Size(0, 13);
            this.lb2_2.TabIndex = 8;
            // 
            // lb1_2
            // 
            this.lb1_2.AutoSize = true;
            this.lb1_2.Location = new System.Drawing.Point(174, 39);
            this.lb1_2.Name = "lb1_2";
            this.lb1_2.Size = new System.Drawing.Size(0, 13);
            this.lb1_2.TabIndex = 7;
            // 
            // lb6_1
            // 
            this.lb6_1.AutoSize = true;
            this.lb6_1.Location = new System.Drawing.Point(24, 200);
            this.lb6_1.Name = "lb6_1";
            this.lb6_1.Size = new System.Drawing.Size(0, 13);
            this.lb6_1.TabIndex = 5;
            // 
            // lb5_1
            // 
            this.lb5_1.AutoSize = true;
            this.lb5_1.Location = new System.Drawing.Point(24, 167);
            this.lb5_1.Name = "lb5_1";
            this.lb5_1.Size = new System.Drawing.Size(0, 13);
            this.lb5_1.TabIndex = 4;
            // 
            // lb4_1
            // 
            this.lb4_1.AutoSize = true;
            this.lb4_1.Location = new System.Drawing.Point(24, 136);
            this.lb4_1.Name = "lb4_1";
            this.lb4_1.Size = new System.Drawing.Size(0, 13);
            this.lb4_1.TabIndex = 3;
            // 
            // lb3_1
            // 
            this.lb3_1.AutoSize = true;
            this.lb3_1.Location = new System.Drawing.Point(24, 104);
            this.lb3_1.Name = "lb3_1";
            this.lb3_1.Size = new System.Drawing.Size(0, 13);
            this.lb3_1.TabIndex = 2;
            // 
            // lb2_1
            // 
            this.lb2_1.AutoSize = true;
            this.lb2_1.Location = new System.Drawing.Point(24, 72);
            this.lb2_1.Name = "lb2_1";
            this.lb2_1.Size = new System.Drawing.Size(0, 13);
            this.lb2_1.TabIndex = 1;
            // 
            // lb1_1
            // 
            this.lb1_1.AutoSize = true;
            this.lb1_1.Location = new System.Drawing.Point(24, 39);
            this.lb1_1.Name = "lb1_1";
            this.lb1_1.Size = new System.Drawing.Size(0, 13);
            this.lb1_1.TabIndex = 0;
            // 
            // FormConsultas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(799, 361);
            this.ControlBox = false;
            this.Controls.Add(this.groupBoxDatos);
            this.Controls.Add(this.gbSeleccion);
            this.Name = "FormConsultas";
            this.Text = "Consultas";
            this.gbSeleccion.ResumeLayout(false);
            this.gbSeleccion.PerformLayout();
            this.groupBoxDatos.ResumeLayout(false);
            this.groupBoxDatos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSeleccion;
        private System.Windows.Forms.ComboBox cbBusqueda;
        private System.Windows.Forms.RadioButton rbAsignatura;
        private System.Windows.Forms.RadioButton rbProfesor;
        private System.Windows.Forms.RadioButton rbAlumno;
        private System.Windows.Forms.GroupBox groupBoxDatos;
        private System.Windows.Forms.Label lb6_2;
        private System.Windows.Forms.Label lb5_2;
        private System.Windows.Forms.Label lb4_2;
        private System.Windows.Forms.Label lb3_2;
        private System.Windows.Forms.Label lb2_2;
        private System.Windows.Forms.Label lb1_2;
        private System.Windows.Forms.Label lb6_1;
        private System.Windows.Forms.Label lb5_1;
        private System.Windows.Forms.Label lb4_1;
        private System.Windows.Forms.Label lb3_1;
        private System.Windows.Forms.Label lb2_1;
        private System.Windows.Forms.Label lb1_1;
    }
}