﻿namespace Colegio
{
    partial class FormAsignaturas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAsignaturas));
            this.btnLimpiarCampos = new System.Windows.Forms.Button();
            this.btnGuardarDatos = new System.Windows.Forms.Button();
            this.pictureBoxAsig = new System.Windows.Forms.PictureBox();
            this.txtNombreAsig = new System.Windows.Forms.TextBox();
            this.txtIdAsig = new System.Windows.Forms.TextBox();
            this.lbHoras = new System.Windows.Forms.Label();
            this.lbDepart = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.comboDep = new System.Windows.Forms.ComboBox();
            this.groupBoxAsigna = new System.Windows.Forms.GroupBox();
            this.txtHorasSemanales = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAsig)).BeginInit();
            this.groupBoxAsigna.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLimpiarCampos
            // 
            this.btnLimpiarCampos.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnLimpiarCampos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpiarCampos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiarCampos.Location = new System.Drawing.Point(519, 292);
            this.btnLimpiarCampos.Name = "btnLimpiarCampos";
            this.btnLimpiarCampos.Size = new System.Drawing.Size(206, 32);
            this.btnLimpiarCampos.TabIndex = 25;
            this.btnLimpiarCampos.Text = "LIMPIAR";
            this.btnLimpiarCampos.UseVisualStyleBackColor = false;
            this.btnLimpiarCampos.Click += new System.EventHandler(this.btnLimpiarCampos_Click);
            // 
            // btnGuardarDatos
            // 
            this.btnGuardarDatos.BackColor = System.Drawing.Color.LightSteelBlue;
            this.btnGuardarDatos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarDatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarDatos.Location = new System.Drawing.Point(231, 292);
            this.btnGuardarDatos.Name = "btnGuardarDatos";
            this.btnGuardarDatos.Size = new System.Drawing.Size(200, 32);
            this.btnGuardarDatos.TabIndex = 24;
            this.btnGuardarDatos.Text = "GUARDAR";
            this.btnGuardarDatos.UseVisualStyleBackColor = false;
            this.btnGuardarDatos.Click += new System.EventHandler(this.btnGuardarDatos_Click);
            // 
            // pictureBoxAsig
            // 
            this.pictureBoxAsig.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxAsig.Image")));
            this.pictureBoxAsig.Location = new System.Drawing.Point(30, 33);
            this.pictureBoxAsig.Name = "pictureBoxAsig";
            this.pictureBoxAsig.Size = new System.Drawing.Size(172, 278);
            this.pictureBoxAsig.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAsig.TabIndex = 23;
            this.pictureBoxAsig.TabStop = false;
            // 
            // txtNombreAsig
            // 
            this.txtNombreAsig.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreAsig.Location = new System.Drawing.Point(167, 78);
            this.txtNombreAsig.Name = "txtNombreAsig";
            this.txtNombreAsig.Size = new System.Drawing.Size(247, 23);
            this.txtNombreAsig.TabIndex = 19;
            // 
            // txtIdAsig
            // 
            this.txtIdAsig.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdAsig.Location = new System.Drawing.Point(167, 37);
            this.txtIdAsig.Name = "txtIdAsig";
            this.txtIdAsig.Size = new System.Drawing.Size(247, 23);
            this.txtIdAsig.TabIndex = 18;
            // 
            // lbHoras
            // 
            this.lbHoras.AutoSize = true;
            this.lbHoras.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHoras.Location = new System.Drawing.Point(13, 170);
            this.lbHoras.Name = "lbHoras";
            this.lbHoras.Size = new System.Drawing.Size(135, 17);
            this.lbHoras.TabIndex = 16;
            this.lbHoras.Text = "Horas Semanales";
            // 
            // lbDepart
            // 
            this.lbDepart.AutoSize = true;
            this.lbDepart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDepart.Location = new System.Drawing.Point(38, 126);
            this.lbDepart.Name = "lbDepart";
            this.lbDepart.Size = new System.Drawing.Size(110, 17);
            this.lbDepart.TabIndex = 15;
            this.lbDepart.Text = "Departamento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(84, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Nombre";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(100, 43);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(33, 17);
            this.lblID.TabIndex = 13;
            this.lblID.Text = "I.D.";
            // 
            // comboDep
            // 
            this.comboDep.FormattingEnabled = true;
            this.comboDep.Location = new System.Drawing.Point(167, 122);
            this.comboDep.Name = "comboDep";
            this.comboDep.Size = new System.Drawing.Size(247, 21);
            this.comboDep.TabIndex = 26;
            // 
            // groupBoxAsigna
            // 
            this.groupBoxAsigna.Controls.Add(this.txtHorasSemanales);
            this.groupBoxAsigna.Controls.Add(this.lblID);
            this.groupBoxAsigna.Controls.Add(this.comboDep);
            this.groupBoxAsigna.Controls.Add(this.label1);
            this.groupBoxAsigna.Controls.Add(this.lbDepart);
            this.groupBoxAsigna.Controls.Add(this.lbHoras);
            this.groupBoxAsigna.Controls.Add(this.txtIdAsig);
            this.groupBoxAsigna.Controls.Add(this.txtNombreAsig);
            this.groupBoxAsigna.Location = new System.Drawing.Point(231, 33);
            this.groupBoxAsigna.Name = "groupBoxAsigna";
            this.groupBoxAsigna.Size = new System.Drawing.Size(494, 244);
            this.groupBoxAsigna.TabIndex = 27;
            this.groupBoxAsigna.TabStop = false;
            this.groupBoxAsigna.Text = "Datos Asignaturas";
            // 
            // txtHorasSemanales
            // 
            this.txtHorasSemanales.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHorasSemanales.Location = new System.Drawing.Point(167, 167);
            this.txtHorasSemanales.Name = "txtHorasSemanales";
            this.txtHorasSemanales.Size = new System.Drawing.Size(116, 23);
            this.txtHorasSemanales.TabIndex = 27;
            // 
            // FormAsignaturas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 361);
            this.ControlBox = false;
            this.Controls.Add(this.groupBoxAsigna);
            this.Controls.Add(this.btnLimpiarCampos);
            this.Controls.Add(this.btnGuardarDatos);
            this.Controls.Add(this.pictureBoxAsig);
            this.Name = "FormAsignaturas";
            this.Text = "Asignaturas";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAsig)).EndInit();
            this.groupBoxAsigna.ResumeLayout(false);
            this.groupBoxAsigna.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLimpiarCampos;
        private System.Windows.Forms.Button btnGuardarDatos;
        private System.Windows.Forms.PictureBox pictureBoxAsig;
        private System.Windows.Forms.TextBox txtNombreAsig;
        private System.Windows.Forms.TextBox txtIdAsig;
        private System.Windows.Forms.Label lbHoras;
        private System.Windows.Forms.Label lbDepart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.ComboBox comboDep;
        private System.Windows.Forms.GroupBox groupBoxAsigna;
        private System.Windows.Forms.TextBox txtHorasSemanales;
    }
}