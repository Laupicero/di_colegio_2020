﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio
{
    //Clase que nos ayudará a la hora de isnertar nuestros alumnos en un fichero
    //Para así no perder la encapsulación
    class AlumnoPOJO
    {
        //Atributos
        private String dni;
        private String nombre;
        private String fechaNac;
        private String direcc;
        private String tlf;

        //Constructores

        //Vacío
        public AlumnoPOJO() { }

        //Con todos los parámetros
        public AlumnoPOJO(string dni, string nombre, string fechaNac, string direcc, string tlf)
        {
            this.dni = dni;
            this.nombre = nombre;
            this.fechaNac = fechaNac;
            this.direcc = direcc;
            this.tlf = tlf;
        }

        //Setter y Getter
        public string DNI { get => dni; set => dni = value; }
        public string NOMBRE { get => nombre; set => nombre = value; }
        public string FECHA_NAC { get => fechaNac; set => fechaNac = value; }
        public string DIRECC { get => direcc; set => direcc = value; }
        public string TLF { get => tlf; set => tlf = value; }
    }
}
