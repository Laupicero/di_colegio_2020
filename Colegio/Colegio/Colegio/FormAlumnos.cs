﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio
{
    public partial class FormAlumnos : Form
    {
        public FormAlumnos()
        {
            InitializeComponent();
        }


        //BOTÓN PARA INSERTAR UN ALUMNO EN EL FICHERO
        private void btnGuardarAlumno_Click(object sender, EventArgs e)
        {
            //Comprobamos que todos los campos esten rellenos y el campo del teléfono también
            if (camposRellenos() && telefonoCorrecto() && dniCorrecto())
            {
                StreamWriter fichero;
                String nombreFichero = "Alumnos.txt";

                //Nos creamos el objeto
                AlumnoPOJO alum = new AlumnoPOJO(txtDniAlumno.Text, txtNombreAlumno.Text, dtpFechaNacAlumno.Value.Date.ToShortDateString(), txtDireccionAlumno.Text, txtTelefonoAlumno.Text);
                //String con las instancias del objeto alum
                String datosAlumno = alum.DNI + "#" + alum.NOMBRE + "#" + alum.FECHA_NAC + "#" + alum.DIRECC + "#" + alum.TLF;

                //Comprobamos si existe el fichero, sino lo creamos
                if (File.Exists(nombreFichero))
                {
                    fichero = File.AppendText(nombreFichero);
                    fichero.WriteLine(datosAlumno);
                    fichero.Close();

                }
                else
                {
                    fichero = File.CreateText(nombreFichero);
                    fichero.WriteLine(datosAlumno);
                    fichero.Close();
                }
                limpiarCampos();
            }
            else
            {
                MessageBox.Show("Compruebe si el formsto del DNI y teléfono son correctos", "¡ERROR!");
            }
            
        }

        




        //BOTÓN PARA LIMPIAR TODOS LOS CAMPOS
        private void btnLimpiarAlumno_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }


        //--------------------
        // MÉTODOS AUXILIARES
        //--------------------

        //nos limpiará todos los campos del formulario
        private void limpiarCampos()
        {
            txtDniAlumno.Clear();
            txtNombreAlumno.Clear();
            dtpFechaNacAlumno.Value = DateTime.Today;
            txtDireccionAlumno.Clear();
            txtTelefonoAlumno.Clear();
        }


        //Nos devuelve un 'true' si están todos los campos rellenos
        private bool camposRellenos()
        {
            bool res = false;
            if (txtDniAlumno.TextLength > 0 && txtNombreAlumno.TextLength > 0 && txtDireccionAlumno.TextLength > 0 && txtTelefonoAlumno.TextLength > 0)
            {
                res = true;
            }
            else
            {
                MessageBox.Show("Debe rellenar primero todos los campos", "Error al insertar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return res;
        }

        //Nos devuelve un 'true' si está correcto el formato del téléfono
        private bool telefonoCorrecto()
        {
            return FiltrosRegex.validarTelefono(txtTelefonoAlumno.Text);
        }


        private bool dniCorrecto()
        {
            return FiltrosRegex.validarDni(txtDniAlumno.Text);
        }


        ////TODO: No ha dado tiempo
        ///Nos devuelve un 'true' si está la fecha correcta
        //private bool fechaCorrecta()
        //{
        //    bool res = false;

        //    //Comparamos si la fecha es mayor que 'hoy'
        //    if (DateTime.Compare(dtpFechaNacAlumno.Value, DateTime.Today) > 0)
        //    {
        //        //Comparamos si la edad del 'alumno' es al menos de 5 años
        //        res = true;
        //    }
        //    else
        //    {
        //        MessageBox.Show("Debe rellenar primero todos los campos", "Error al insertar", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    return res;
        //}



    }
}
