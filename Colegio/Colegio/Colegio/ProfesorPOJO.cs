﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio
{
    //Clase que nos ayudará a la hora de isnertar nuestros profesores en un fichero
    //Para así no perder la encapsulación
    class ProfesorPOJO
    {
        //Atributos
        private String dni;
        private String nombre;
        private String dirrecc;
        private string tlf;
        private String estudios;
        private String asignaturaImpartida;

        //Constructores

        //Constructor vacío
        public ProfesorPOJO(){}

        //Constructor por parámetros
        public ProfesorPOJO(string dni, string nombre, string dirrecc, string tlf, string estudios, string asignaturaImpartida)
        {
            this.dni = dni;
            this.nombre = nombre;
            this.dirrecc = dirrecc;
            this.tlf = tlf;
            this.estudios = estudios;
            this.asignaturaImpartida = asignaturaImpartida;
        }

        //Getters Y Setters
        public string DNI { get => dni; set => dni = value; }
        public string NOMBRE { get => nombre; set => nombre = value; }
        public string DIRECCION { get => dirrecc; set => dirrecc = value; }
        public string TLF { get => tlf; set => tlf = value; }
        public string ESTUDIOS { get => estudios; set => estudios = value; }
        public string ASIGNATURA_IMPARTIDA { get => asignaturaImpartida; set => asignaturaImpartida = value; }
    }
}
