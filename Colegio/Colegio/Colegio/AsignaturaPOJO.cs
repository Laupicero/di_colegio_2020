﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio
{
    class AsignaturaPOJO
    {
        //Atributos
        private String id;
        private String nombre;
        private String departamento;
        private String horasSemanales;

        //CONSTRUCTORES
        //Constructor vacío

        public AsignaturaPOJO() { }

        //Constructor por parámetros
        public AsignaturaPOJO(string id, string nombre, string departamento, string horasSemanales)
        {
            this.id = id;
            this.nombre = nombre;
            this.departamento = departamento;
            this.horasSemanales = horasSemanales;
        }

        //Getters y Setters
        public string ID { get => id; set => id = value; }
        public string NOMBRE { get => nombre; set => nombre = value; }
        public string DEPARTAMENTO { get => departamento; set => departamento = value; }
        public string HORAS_SEMANALES { get => horasSemanales; set => horasSemanales = value; }
    }
}
